package com.example.myback;

import com.example.myback.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
public class MyBackApplication {
    public static void main(String[] args) {
        SpringApplication.run(MyBackApplication.class, args);
    }

}
